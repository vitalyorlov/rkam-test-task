package com.example.vitalyorlov.rkamtestapplication.utils;

import java.util.ArrayList;

public class ImagesContainer {
    public static ArrayList<Integer> uploadedImageList;
    private static ArrayList<String> imageList;
    public static int uploadedImagesCount;

    private ImagesContainer() {}

    static {
        uploadedImageList = new ArrayList<Integer>();
        imageList = new ArrayList<String>();

        imageList.add("http://www.omega-shops.ru/upload/iblock/d7d/d7d756ab27a4edaf89483c6af4fc2983.jpg");
        imageList.add("http://yabee.ru/wp-content/uploads/2016/02/50043968b.jpg");
        imageList.add("http://opt-585832.ssl.1c-bitrix-cdn.ru/upload/medialibrary/772/7728fb5d69b6b21c149a60920ad9e241.jpg?14449806481135599");
        imageList.add("http://velozavr.com.ua/image/cache/data/velosumki-velochehli/sumki/0/-obr-online-katalog-399030-600x600.jpg");
        imageList.add("http://i4.sravni.ua/images/model/786/681621/2/3/type1/kuboq_kqapip5skf1bkpulc_794341.jpg");
        imageList.add("http://velootdyh.ru/data/images/img/556_image.jpg");
        imageList.add("http://fast.ulmart.ru/p/ym/375/37544/3754492.jpg");
        imageList.add("http://krutmobile.reshop.com.ua/user/catalog/311/smart-watch-gt08-apple-design_1_original.jpg");
        imageList.add("http://i-world.com.ua/image/data/BY8/iphone5__0001_edit_merged_.jpg");
        imageList.add("https://krasnoyarsk.e2e4online.ru/price/stock/vergeb_06_full1000x1000.jpg");
        imageList.add("http://www.mechta.kz/export/1cbitrix/import_files/fc/fc14e71a-ea9b-11e5-9ec9-2c768a5d3551.jpeg");
        imageList.add("http://static12.insales.ru/images/products/1/1645/2942573/iPhone_4GS_W99_.jpg");
        imageList.add("http://x-point.ru/files/imagecache/fullsize/product.42495a.jpg");
        imageList.add("http://galaxy-droid.ru/uploads/posts/2016-02/1455008813_samsung-galaxy-s7-s7-edge-waterproof-components-05.png");
        imageList.add("http://www.dustroads.ru/published/publicdata/DUST2014190515/attachments/SC/products_pictures/SMART-MAXI_enl.jpeg");
        imageList.add("http://en1.data.coolicool.com/images/PAMB268681/S960SX/D3_1.jpg");
        imageList.add("http://primuss.ru.images.1c-bitrix-cdn.ru/upload/iblock/e0c/HTB1EuorFVXXXXaFXpXXq6xXFXXXH.jpg?143587065352693");
        imageList.add("http://www.je-store.ru/upload/catalog/237/12269_51.jpg");
        imageList.add("http://www.icover.ru/upload/iblock/3d8/3d8cdd21575ea8b79be3381dd96d30da.jpg");
        imageList.add("http://iflashco.ru/assets/images/Griffin/iPhone-5/i5griffin-24.png");

    }

    public static ArrayList<String> getImageList() {
        return imageList;
    }
}
