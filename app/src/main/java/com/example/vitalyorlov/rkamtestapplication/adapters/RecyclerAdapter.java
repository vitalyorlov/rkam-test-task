package com.example.vitalyorlov.rkamtestapplication.adapters;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.vitalyorlov.rkamtestapplication.R;
import com.example.vitalyorlov.rkamtestapplication.utils.ImageDownloaderTask;
import com.example.vitalyorlov.rkamtestapplication.utils.ImagesContainer;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private ArrayList<String> mDataset;
    private Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImageView;

        public ViewHolder(View v) {
            super(v);
            mImageView = (ImageView) v.findViewById(R.id.imageView);
        }
    }

    public RecyclerAdapter(ArrayList<String> dataset, Context context) {
        mDataset = dataset;
        this.context = context;
    }

    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    private void showAlert(Context context) {
        new AlertDialog.Builder(context)
                .setMessage("Download complete")
                .show();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder.mImageView != null) {
            //new ImageDownloaderTask(holder.mImageView, context).execute(mDataset.get(position));
            Picasso.with(context)
                    .load(mDataset.get(position))
                    .resize(400, 400)
                    .into(holder.mImageView, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            ImagesContainer.uploadedImagesCount++;

                            if (ImagesContainer.uploadedImagesCount == ImagesContainer.getImageList().size())
                                showAlert(context);
                        }

                        @Override
                        public void onError() {

                        }
                    });
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
