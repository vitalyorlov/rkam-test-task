package com.example.vitalyorlov.rkamtestapplication.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.vitalyorlov.rkamtestapplication.R;
import com.example.vitalyorlov.rkamtestapplication.adapters.RecyclerAdapter;
import com.example.vitalyorlov.rkamtestapplication.utils.ImagesContainer;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new RecyclerAdapter(ImagesContainer.getImageList(), MainActivity.this);
        mRecyclerView.setAdapter(mAdapter);
    }

}
